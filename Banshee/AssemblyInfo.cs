using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("banshee-plugin-mirage")]
[assembly: AssemblyDescription("Automatic Playlist Generation Extension for the Banshee Music Player")]
[assembly: AssemblyVersion("0.6.0")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]

