using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("mirage")]
[assembly: AssemblyDescription("Music Similarity and Automatic Playlist Generation library")]
[assembly: AssemblyVersion("0.6.0")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]

